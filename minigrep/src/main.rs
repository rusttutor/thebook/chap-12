use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    // println!("{:?}", args);

    let config = Config::new(&args[1..]);
    println!("Searching for {}", config.query);
    println!("In file {}", config.filename);

    let contents = fs::read_to_string(config.filename).expect("Faild to read the file");

    println!("With contents:\n{}", contents);
}

struct Config {
    query: String,
    filename: String,
}

impl Config {
    fn new(args: &[String]) -> Config {
        if args.len() < 2 {
            panic!("Need two arguments!");
        }
        let query = args[0].clone();
        let filename = args[1].clone();

        Config { query, filename }
    }
}

// fn parse_config(args: &[String]) -> Config {
//     let query = args[0].clone();
//     let filename = args[1].clone();

//     Config { query, filename }
// }
